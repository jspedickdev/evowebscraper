from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup
from openpyxl import load_workbook
import string

def DownloadHtmlPage(my_url):
    uClient = uReq(my_url)
    page_html = uClient.read()
    uClient.close()
    return page_html


def GetNumberOfColumns(sheet):
    count = 1
    while(sheet.cell(row=1, column=count).value is not None):
        count = count + 1
    return count - 1


def FindColumn(sheet, columnName):
    col = 1
    while(sheet.cell(row=1, column=col).value is not None):
        if(sheet.cell(row=1, column=col).value == columnName):
             return col
        else:
             col += 1
    return 0

def GetNextAvailableRow(sheet, columnName):
    row = 1
    columnMatch = FindColumn(sheet, columnName)
    if(columnMatch == 0):
        return row
    else:
        while(sheet.cell(row=row, column=columnMatch).value is not None):
            row += 1
        return row
            

def WriteColumn(sheet, columnName, column, index):
    row = GetNextAvailableRow(sheet, columnName)
    if(row == 1):
        sheet.cell(row = row, column =index).value = columnName
        row += 1
    for val in column:
        sheet.cell(row = row, column = index).value = val
        row += 1
    return
    

                    
def SaveToExcel(columnName, column):
    saveFile = "SnowboardingWebScraper.xlsx"
    wb = load_workbook(filename = saveFile)
    sheet = wb["Evo"]
    numberOfColumns = GetNumberOfColumns(sheet)
    columnMatch = FindColumn(sheet, columnName)
    if(columnMatch == 0):
        WriteColumn(sheet, columnName, column, numberOfColumns + 1)
    else:
        WriteColumn(sheet, columnName, column, columnMatch)
    wb.save(saveFile)

def CrawlEvoCatalog(evoCatalogHtml):
    productThumbsHtml = evoCatalogHtml.findAll("div",{"class":"product-thumb-details"})
    count = 0 
    for productThumbHtml in productThumbsHtml:
        if(count > 1):
            break
        try:
            snowboardProductLinkRoute = productThumbHtml.a["href"]
            #snowboardProductLink = base_url + snowboardProductLinkRoute
            #snowboardProductLinkHtml = DownloadHtmlPage(snowboardProductLink)
            downloadedEvoCatalogHtml =  "..\html\Rome Factory Rocker Snowboard 2017 _ evo.html"
            snowboardProductHtmlExample = soup(open(downloadedEvoCatalogHtml), "html.parser")
            ParseProductDetails(snowboardProductHtmlExample)
        except:
            print('error occured parsing snowboard product details' + downloadedEvoCatalogHtml)

def ParseProductDetails(snowboardDetailsHtml):
    ''' Consider using find next to optimize parsing of page
        That would make the searching order dependent but not
        require that we start from the top everytime'''
    try:
        snowboardHeader = snowboardDetailsHtml.find("h1",{"class":"pdp-header-title"})
        #Parse Brand
        snowboardBrand = snowboardHeader.find(itemprop='name').contents[0]
        #Parse Name
        snowboardName = snowboardHeader.contents[1]
        #Parse Summary
        snowboardSummaryContents = snowboardDetailsHtml.find(itemprop='description').contents
        snowboardSummary = ""
        for summaryContent in snowboardSummaryContents:
            if isinstance(summaryContent,str):
                snowboardSummary = snowboardSummary + summaryContent #need to replace this with an if statment to use the contains substring of '<' to exclude html content

        specStrings = {0:"Rocker Type",
                 1:"Shape",
                 2:"Core",
                 3:"Laminates",
                 4:"Base",
                 5:"Binding Compatibility"
        }
        
        specsHeader = snowboardDetailsHtml.find(string="Rocker Type").find_parent("div")

        SpecType = specsHeader.findNext("h5").string
        SpecName = specsHeader.find_next_sibling("em").string
        SpecSummary = specsHeader.find_next_sibling("p").string
        specsHeader = specsHeader.find_next_sibling("h5")

    except:
        print('error occured parsing product details for ' + snowboardDetailsHtml)

    #SaveToExcel("Brand",snowboardBrands)
    #SaveToExcel("Model",snowboardNames)
        

# Main Function
downloadedEvoCatalogHtml = "..\html\Snowboards.html"
evoCatalogHtml = soup(open(downloadedEvoCatalogHtml), "html.parser")
CrawlEvoCatalog(evoCatalogHtml)
