from urllib.request import urlopen, Request
from bs4 import BeautifulSoup as soup
from openpyxl import load_workbook
import string
import sys
import os

def DownloadHtmlPage(my_url):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.3'}
    reg_url = my_url
    req = Request(url=reg_url, headers=headers) 
    page_html = urlopen(req).read()
    return page_html

def GetNumberOfRows(sheet):
    count = 1
    while(sheet.cell(row=count, column=1).value is not None):
        count = count + 1
    return count - 1

def GetNextAvailableRow(saveFile):
    wb = load_workbook(filename = saveFile)
    sheet = wb["Evo"]
    numberOfRows = GetNumberOfRows(sheet)
    return numberOfRows + 1

def WriteHeaderRowToFile(inputAttributes):
    saveFile = "EvoWebScraper.xlsx"
    wb = load_workbook(filename = saveFile)
    sheet = wb["Evo"]
    row = 1
    col = 1
    for spec in inputAttributes:
        sheet.cell(row = row, column = col).value = spec
        col += 1
    wb.save(saveFile)

def WriteRowToFile(row, specDictionary, inputAttributes):
    saveFile = "EvoWebScraper.xlsx"
    wb = load_workbook(filename = saveFile)
    sheet = wb["Evo"]
    colNum = 0
    for spec, value in specDictionary.items():
        if(spec.lower() in inputAttributes):
            colNum = inputAttributes.index(spec.lower()) + 1
            sheet.cell(row = row, column = colNum).value = value
        else:
            print("Ignoring Spec: "+ spec)
    wb.save(saveFile)
    

def WriteSnowboardToExcel(specDictionary, inputAttributes):
    saveFile = "EvoWebScraper.xlsx"
    WriteHeaderRowToFile(inputAttributes)
    availableRow = GetNextAvailableRow(saveFile)
    WriteRowToFile(availableRow, specDictionary, inputAttributes)

def GetInputAttributes():
    inputAttributes = [ 'hyperlink',
                                'name',
                                'summary',
                                'terrain',
                                'ability level',
                                'rocker type',
                                'shape',
                                'core/laminates',
                                'flex rating',
                                'binding mount pattern',
                                'warranty',
                                'made in the usa']
    return inputAttributes
    

def CrawlEvoCatalog(evoCatalogHtml):
    base_url = "https://www.evo.com"
    inputAttributes = GetInputAttributes()
    productThumbsHtml = evoCatalogHtml.findAll("div",{"class":"product-thumb-details"})
    count = 1
    for productThumbHtml in productThumbsHtml:
        if(count < 1):
            break
        try:
            snowboardProductLinkRoute = productThumbHtml.a["href"]
            snowboardProductLink = base_url + snowboardProductLinkRoute
            snowboardProductLinkHtml = soup(DownloadHtmlPage(snowboardProductLink),"html.parser")
            #downloadedEvoCatalogHtml =  "..\html\Rome Factory Rocker Snowboard 2017 _ evo.html"
            #snowboardProductHtmlExample = soup(open(downloadedEvoCatalogHtml), "html.parser")
            specDictionary = ParseProductDetails(snowboardProductLinkHtml, snowboardProductLink)
            WriteSnowboardToExcel(specDictionary, inputAttributes)
            print("Successfully wrote " + snowboardProductLinkRoute + " to file")
            count = count + 1
        except FileNotFoundError as err:
            print("Excel Save File Not Found")
        except :
            print('error occured parsing snowboard product details' + snowboardProductLink)
            print("Unexpected error:", sys.exc_info()[0])
            raise

def ParseProductDetails(snowboardDetailsHtml, snowboardProductLink):
    ''' Consider using find next to optimize parsing of page
        That would make the searching order dependent but not
        require that we start from the top everytime'''
    try:
        snowboardHeader = snowboardDetailsHtml.find("h1",{"class":"pdp-header-title"})
        #Parse Brand
        # snowboardBrand = snowboardHeader.find(itemprop='name').contents[0]
        #Parse Name
        snowboardName = snowboardHeader.contents[0].string.replace('\n','')
        #Parse Summary
        snowboardSummaryContents = snowboardDetailsHtml.find(itemprop='description').contents
        snowboardSummary = ""
        for summaryContent in snowboardSummaryContents:
            if isinstance(summaryContent,str):
                if "href" not in summaryContent:
                    snowboardSummary = snowboardSummary + summaryContent

        specDictionary = {'HyperLink': snowboardProductLink,
                          'Name' : snowboardName,
                          'Summary': snowboardSummary
                         }
        specsModule = snowboardDetailsHtml.find("ul",{"class":"pdp-spec-list"}).findAll("li")
        for spec in specsModule:
            specsName = spec.find("span",{"class":"pdp-spec-list-title"}).string.replace(':','')
            specValue = spec.find("span",{"class":"pdp-spec-list-description"}).string.replace(':','')
            specDictionary[specsName] = specValue

        return specDictionary
    except:
        print('error occured parsing product details for ' + snowboardProductLink)

    #SaveToExcel("Brand",snowboardBrands)
    #SaveToExcel("Model",snowboardNames)
        

# Main Function
evoSnowboardsUrl = "https://www.evo.com/shop/snowboard/snowboards"
evoSnowboardsHtml = soup(DownloadHtmlPage(evoSnowboardsUrl),"html.parser")
CrawlEvoCatalog(evoSnowboardsHtml)
#downloadedEvoCatalogHtml = "..\html\Snowboards.html"
#testPage = DownloadHtmlPage(my_url)
#testSoup = soup(testPage, "html.parser")
#ParseProductDetails(testSoup)
#evoCatalogHtml = soup(open(downloadedEvoCatalogHtml), "html.parser")
#CrawlEvoCatalog(evoCatalogHtml)
